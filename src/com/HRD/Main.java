package com.HRD;

import java.util.Collection;
import java.util.Iterator;
import java.util.TreeSet;

public class Main {
	public static void main(String[] args) {
		MyList<String> list = new MyList<>();
		try {
            list.addItem("k");
            list.addItem("k");

//          list.addItem(null);     //java will handle this exception
// Once my program catch any handling exception, it doesn't execute the below code anymore.
        }catch (Exception e){
            System.out.println(e);
        }
		System.out.println("\nList of all Inputted: \n");
		for (int i = 0; i < list.size(); i++) {
            System.out.println(list.getItem(i));
		}
		}
}
