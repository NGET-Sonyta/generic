package com.HRD;

public class NulliException extends Exception {
    public NulliException() {
        super();
    }

    public NulliException(String message) {
        super(message);
    }
}
