package com.HRD;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class MyList<E> {
    private List list=new LinkedList<E>();
    public void addItem(E value) throws DuplicateException, NulliException {
        if(value.equals(null)){
            throw new NulliException("\nSorry, the Value cannot Be NULL.");
        }
        for (int i = 0; i < list.size();  i++) {
            if (list.get(i) == value) {
                throw new DuplicateException("\nDuplicated Value is: " + value);
            }
        }
//        int size=list.length;
//        Object temp[]=list;
//        list=new Object[size+1];
//        for (int i=0;i<temp.length;i++) {
//            list[i] = temp[i];
//        }
        list.add(value);
    }
    public E getItem(int index){
        return (E)list.get(index);
    }
    public int size(){
        return list.size();
    }
}
